# extend-mapper

#### 介绍
基于通用Mapper基础上写的扩展，类似于jpa。

#### 软件架构
框架     | 版本
-------- | -----
通用Mapper  | 2.1.3
lombok | 1.18.4
spring-boot | 2.1.2.RELEASE


#### 安装教程

1. 克隆项目到本地，并转换为maven项目。
2. 导入子项目。
3. 右键extend-mapper，运行Run as -> Maven build，输入 clean package，点击运行。