package online.inote.mapper.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * Desc: 生成Mapper注解类
 * </p>
 *
 * @author Sui
 * @created: 2017年12月9日 下午4:03:17
 * @version 1.0
 */
@Target(ElementType.TYPE) 
@Retention(RetentionPolicy.RUNTIME)
public @interface GenerateMapper {

	/**
	  * <p>
	  * Desc: 是否生成对应的Service
	  * </p>
	  *
	  * @author Sui
	  * @created 2017年12月10日 下午8:50:50
	  * @return
	 */
	boolean isGenerateService() default false;
	
	String serviceName() default "";
	
	/**
	  * <p>
	  * Desc: mapper的生成路径
	  * </p>
	  *
	  * @author Sui
	  * @created 2017年12月10日 下午8:51:13
	  * @return
	 */
	String generateMapperPath() default "";
	
	String mapperName() default "";
	
	String signatureSuffix() default "Entity";
	
	/**
	  * <p>
	  * Desc: service的生成路径
	  * </p>
	  *
	  * @author Sui
	  * @created 2017年12月10日 下午8:51:30
	  * @return
	 */
	String generateServicePath() default "";
	
	/**
	  * <p>
	  * Desc: 如果配置多数据源或修改sqlSessionFactoryBeanName的话, 需要指定
	  * </p>
	  *
	  * @author Sui
	  * @created 2017年12月10日 下午8:52:45
	  * @return
	 */
	String sqlSessionFactoryBeanName() default "sqlSessionFactory";
	
	String transactionManager() default "transactionManager";
}
