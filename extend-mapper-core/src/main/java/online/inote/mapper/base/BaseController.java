package online.inote.mapper.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseController<T extends IdEntity> {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	protected BaseService<T> service;
}