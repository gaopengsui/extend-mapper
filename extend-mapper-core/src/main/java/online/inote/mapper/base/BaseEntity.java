package online.inote.mapper.base;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
@EqualsAndHashCode(callSuper = false)
public class BaseEntity extends IdEntity {

	private static final long serialVersionUID = -2399167847871573320L;

	private Date createTime;

	public void initCreateTime() {
		this.setCreateTime(new Date());
	}
}