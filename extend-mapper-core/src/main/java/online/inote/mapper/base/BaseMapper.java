package online.inote.mapper.base;

import tk.mybatis.mapper.common.Mapper;

/**
 * <p>
 * Desc: 基础Mapper
 * </p>
 *
 * @author XQF SUI
 * @created 2019年1月10日 下午4:32:05
 * @version 1.0
 */
public interface BaseMapper<T> extends Mapper<T> {

}