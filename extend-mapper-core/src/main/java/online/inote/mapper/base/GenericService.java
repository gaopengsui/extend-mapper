package online.inote.mapper.base;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class GenericService<T> {

	protected final Class<T> clazz;
	
	@SuppressWarnings("unchecked")
	public GenericService() {
		Type type = getClass().getGenericSuperclass();
		Type[] params = ((ParameterizedType) type).getActualTypeArguments();
		clazz = (Class<T>) params[0];
	}
}