package online.inote.mapper.base;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Id;

import lombok.Data;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
public class IdEntity implements Serializable {

	private static final long serialVersionUID = -5572745710936673402L;

	@Id
	private String Id;
	
	public String generatedId() {
		this.setId(UUID.randomUUID().toString().replace("-", ""));
		return this.Id;
	}
}
