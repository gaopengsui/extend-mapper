package online.inote.mapper.base;

import org.springframework.beans.factory.annotation.Value;

public class TestService {
	
	@Value("${online.inote.mapper.service-package}")
	private String aa;

	public String test() {
		return aa;
	}
}