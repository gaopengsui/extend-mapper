package online.inote.mapper.core;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;
import online.inote.mapper.base.BaseMapper;
import online.inote.mapper.base.BaseService;

/**
 * <p>
 * Desc: 
 * </p>
 *
 * @author Sui
 * @created: 2017年12月10日 下午7:48:52
 * @version 1.0
 */
@Data
@ConfigurationProperties(prefix = "online.inote.mapper")
public class ExtendMapperProperties {

	public static final String PREFIX = "online.inote.mapper";
	
	/**
	 * 扫描指定包下的model
	 */
	private String[] scanModelPaths;
	
	/**
	 * 扫描指定包下已存在的Mapper
	 */
	private String[] scanMapperPaths;
	
	/**
	 * 扫描指定包下已存在的Service
	 */
	private String[] scanServicePaths;
	
	private Class<?> superMapper = BaseMapper.class;
	
	private Class<?> superService = BaseService.class;
}