package online.inote.mapper.utils;

public class ArraysUtils {

	public static boolean isEmpty(Object[] obj) {
		return obj == null || obj.length == 0;
	}
}