package online.inote.mapper.utils;

import org.slf4j.helpers.MessageFormatter;
import org.springframework.lang.Nullable;

/**
 * <p>
 * Desc: String 工具类
 * </p>
 *
 * @author Sui
 * @created: 2017年12月2日 下午11:59:18
 * @version 1.0
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {

	/**
	  * <p>
	  * Desc: 处理参数输入
	  * </p>
	  *
	  * @author Sui
	  * @created 2017年12月2日 下午11:59:43
	  * @param message
	  * @param o
	  * @return
	  */
	public static String initMessage(String message, Object[] o) {
		return MessageFormatter.arrayFormat(message, o).getMessage();
	}

	public static boolean hasText(@Nullable String str) {
		return (str != null && !str.isEmpty() && containsText(str));
	}
	
	private static boolean containsText(CharSequence str) {
		int strLen = str.length();
		for (int i = 0; i < strLen; i++) {
			if (!Character.isWhitespace(str.charAt(i))) {
				return true;
			}
		}
		return false;
	}

	public static boolean hasLength(@Nullable String str) {
		return (str != null && !str.isEmpty());
	}
}
